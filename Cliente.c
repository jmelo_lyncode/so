#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include "Commons.h"

int fw = 0;

void reverseString (char *str) {
    int i, j, len;
	char temp;
	i=j=len=temp=0;

	len=strlen(str);
	for (i=0, j=len-1; i<=j; i++, j--)
	{
		temp=str[i];
		str[i]=str[j];
		str[j]=temp;
	}
}

void PipeS(int s){
    if (SIGPIPE) fw = -1;
}

int main(){

	char *line, *comando, *copia, *valor, *arg;
	int ficheiroL, ficheiroT;
    int flag = 1;

	fw = open("client", O_WRONLY | O_NONBLOCK);

    ficheiroL = open("serverLog.txt", O_WRONLY | O_APPEND);

    signal(SIGPIPE,PipeS);

    while((line = nextLine(0))){
        fw = open("client", O_WRONLY | O_NONBLOCK);

        copia = strdup(line);

        comando = strsep(&copia, " ");

        if (strcmp(comando, "RunTest") == 0) {

            ficheiroT = open("test.txt", O_RDWR | O_APPEND);

            while((line = nextLine(ficheiroT)) != NULL){
                write(fw, line, strlen(line));
                free(line);
                line=NULL;
            }
            flag = 0;
        }

        if(fw != -1 && flag){
            writeLine(fw, line);
            free(line);
        }
        else if (flag){
            if(ficheiroL == -1) {
                creat("serverLog.txt", 0666);
                ficheiroL = open("serverLog.txt", O_WRONLY | O_APPEND);
                writeLine(ficheiroL, line);
            }
            else {
                writeLine(ficheiroL, line);
            }
        }
        flag = 1;
    }
    free(line);
}
