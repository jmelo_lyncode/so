
CC = gcc

CFLAGS = -g -ansi -Wall -Wextra -Wno-unused-result -pedantic -O3 -std=c99

all: Dados.o Servidor Cliente

Commons.o : Commons.c Commons.h
	$(CC) -o Commons.o -c Commons.c

Dados.o : Dados.c Dados.h
	$(CC) -o Dados.o -c Dados.c $(CFLAGS)

Servidor : Servidor.c Dados.o Commons.o
	$(CC) Dados.o Commons.o Servidor.c -o Servidor $(CFLAGS)

Cliente : Cliente.c Commons.o
	$(CC) Commons.o Cliente.c -o Cliente $(CFLAGS)

runServer: Servidor
	./Servidor

runClient: Cliente
	./Cliente

clean :
	rm -f Commons.o
	rm -f Dados.o
	rm -f Servidor
	rm -f Cliente
	rm -f client
	rm -f server
