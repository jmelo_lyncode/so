
#include "Dados.h"
#include "Commons.h"

/* ----------------------------------------------------------------------------------- */

char *tolower_line (char *string){
	int i = strlen(string);

	if (!string) return NULL;

	while (i>=0) {
        string[i] = tolower(string[i]);
        i--;
	}
	return string;
}

void reverseString (char *str) {
    int i, j, len;
	char temp;
	i=j=len=temp=0;

	len=strlen(str);
	for (i=0, j=len-1; i<=j; i++, j--)
	{
		temp = str[i];
		str[i] = str[j];
		str[j] = temp;
	}
}

/**
 * This function will work with a mutex (mutual exclusion).
 * Basically, it should call the incrementar function
 * surrounded by the mutex code.
 *
 * Returns 0 if success or 1 if something went wrong.
 */
int callIncrementar (char *line){
    char *nomes[4];
    int valor;
    char *comando, *inteiro;
    char *distrito, *concelho, *freguesia;
    char *copia = strdup(line);


    comando = strsep(&copia, " "); // Comando contém o apontador para a 1ª posição (free)
    distrito = strsep(&copia, ":");
    concelho = strsep(&copia, ":");
    freguesia = strsep(&copia, ":");

    valor = atoi(copia);

    nomes[0] = strdup(tolower_line(distrito));
    nomes[1] = strdup(tolower_line(concelho));
    nomes[2] = strdup(tolower_line(freguesia));
    nomes[3] = NULL;


    free(comando);

    return incrementar(nomes, valor);
}


/**
 * This function will work with a mutex (mutual exclusion).
 * Basically, it should call the agrega function
 * surrounded by the mutex code.
 *
 * Returns 0 if success or 1 if something went wrong.
 */
int callAgregar(char *line){
    char *nomes[4];
    int valor;
    char *path;
    char *comando, *inteiro;
    char *distrito, *concelho;
    char *copia;

    copia = strdup(line);

    comando = strsep(&copia, " ");

    //free(comando);

    reverseString(copia);

    path = strsep(&copia, " ");

    inteiro = strsep(&copia, " ");

    reverseString(copia);
    reverseString(inteiro);
    reverseString(path);

    valor = atoi(inteiro);

    //free(inteiro);

    distrito = strsep(&copia, ":");

    if (!copia) {
        nomes[0] = strdup(tolower_line(distrito));
        nomes[1] = NULL;
        nomes[2] = NULL;
        nomes[3] = NULL;

        //free(distrito);

        return agregar(nomes,valor,path);
    }

    concelho = strsep(&copia, ":");

    if (!copia) {
        if (valor > 0) {
            nomes[0] = strdup(tolower_line(distrito));
            nomes[1] = strdup(tolower_line(concelho));
            nomes[2] = NULL;
            nomes[3] = NULL;

            //free(distrito);
            //free(concelho);
            //free(copia);

            return agregar(nomes,valor,path);
        }
    }

    if (valor > 1) {
        nomes[0] = strdup(tolower_line(distrito));
        nomes[1] = strdup(tolower_line(concelho));
        nomes[2] = strdup(tolower_line(copia));
        nomes[3] = NULL;

        //free(distrito);
        //free(concelho);
        //free(copia);

        return agregar(nomes,valor,path);
    }
    return 0;
}

int safeFork () {
    int pid = fork();
    if (pid == -1)
        _exit(1); // Currently unable to create child processes. So I'm blowing up!
    return pid;
}


int isValidInput (char *line) {
	return line != NULL && strlen(line) > 0;
}

/**
 * This function does not care about concurrency.
 * It just processes the input it receives and passes
 * the storage to the storage function.
 *
 * This function should also free the given input.
 *
 * Returns 0 if success, 1 if something went wrong.
 **/
int process(char *input) {
		if (startsWith(input, "incrementar"))
				callIncrementar(input);
		else if (startsWith(input, "agrega"))
				callAgregar(input);

    free(input);
    return 0;
}


/**
 * This function starts the server, listening on the specified
 * named pipe. It reads one entire line from the named pipe
 * and stores it in a log file.
 * It also creates a fork of the process passing as it's input
 * the same line.
 */
int server (char *pipeName) {
    int pid = 0;
    char *line = NULL;
		int input = open(pipeName, O_RDONLY);
		int ficheiroS = open("server.txt", O_RDWR | O_APPEND);

    if (ficheiroS == -1) {
        creat("server.txt", 0666);
        ficheiroS = open("server.txt", O_RDWR | O_APPEND);
    }

    while (1) {
        line = nextLine(input);
				if (isValidInput(line)) {
        	writeLine(ficheiroS, line);
					pid = safeFork();
					if (!pid) // sou o filho
						return process(line); // Vou processar e terminar logo de seguida
				}
    }
    close(ficheiroS);
}

int main(){
    int ficheiroL, ficheiroS;
    char *line;
    mkfifo("client", 0777);

    /* Carregar Servidor */
    ficheiroS = open("server.txt", O_RDWR | O_APPEND);
    ficheiroL = open("serverLog.txt", O_RDWR | O_APPEND);

    if(ficheiroS == -1) {
        creat("server.txt", 0666);
        ficheiroS = open("server.txt",O_RDWR | O_APPEND);
    }
    else {
        while((line = nextLine(ficheiroS)) != NULL){
            	process(line);
        }
    }

    if(ficheiroL == -1) {
        creat("serverLog.txt", 0666);
        ficheiroL = open("serverLog.txt",O_RDWR | O_APPEND);
    }
    else {
        while((line = nextLine(ficheiroL)) != NULL){
          	process(line);
        }
        remove("serverLog.txt");
        creat("serverLog.txt", 0666);
        ficheiroL = open("serverLog.txt",O_RDWR | O_APPEND);
    }
    close(ficheiroL);
    close(ficheiroS);

    server("client");

}
