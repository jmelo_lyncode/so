
#include "Dados.h"

/* Fator de Balanceamento */
typedef enum balancefactor { LH , EH , RH } BalanceFactor;

/* Nodo da árvore de freguesias */
typedef struct treeFreg {
    BalanceFactor bf;

    char *freguesia; /* Nome da freguesia */
    int valor;

    struct treeFreg *left;
    struct treeFreg *right;
} FregNode;

/* Nodo da árvore concelhos */
typedef struct treeConc {
    BalanceFactor bf;

    char *concelho; /* Nome do concelho */
    int agregadoC;

    struct treeFreg *freguesias;

    struct treeConc *left;
    struct treeConc *right;
} ConcNode;

typedef struct distrito {

    char *distrito; /* Nome do distrito */
    int agregadoD;
    struct treeConc *concelhos;

} DistNode;

typedef struct treeFreg *Freg;
typedef struct treeConc *Conc;
typedef struct distrito *Dist;

static Dist distritos[18];
static int livre = 0;

static Freg insertTreeF(Freg t, char *e, int valor, int *cresceu);

static Conc insertTreeC(Conc t, char *e[], int valor, int *cresceu);

/* ----------------------------------------------------------------------------------- */


/* Roda uma árvore á direita */
static Freg rotateRightF(Freg t) {
    Freg aux;

    if ((! t) || (! t->left)) {
       printf("Erro\n");
    }

    else {
        aux = t->left;
        t->left = aux->right;
        aux->right = t;
        t = aux;
    }
    return t;
}

/* Roda uma árvore á esquerda */
static Freg rotateLeftF(Freg t) {
    Freg aux;

    if ((! t)||(! t->right)) {
       printf("Erro\n");    }

    else {
        aux = t->right;
        t->right = aux->left;
        aux->left = t;
        t = aux;
    }
    return t;
}

/* Roda uma árvore para que esta fique balanceada rotação a esquerda */
static Freg balanceRightF(Freg t) {

    if (t->right->bf==RH) {
        /* Rotacao simples a esquerda */
        t = rotateLeftF(t);
        t->bf = EH;
        t->left->bf = EH;
    }

    else {
        /* Dupla rotacao */
        t->right = rotateRightF(t->right);
        t = rotateLeftF(t);

        switch (t->bf) {
            case EH:
                t->left->bf = EH;
                t->right->bf = EH;
                break;
            case LH:
                t->left->bf = EH;
                t->right->bf = RH;
                break;
            case RH:
                t->left->bf = LH;
                t->right->bf = EH;
        }
        t->bf = EH;
    }
    return t;
}

/* Roda uma árvore para que esta fique balanceada rotação a direita*/
static Freg balanceLeftF(Freg t) {

    if (t->left->bf==LH) {
        /* Rotacao simples a direita */
        t = rotateRightF(t);
        t->bf = EH;
        t->right->bf = EH;
    }

    else {
        /* Dupla rotacao */
        t->left = rotateLeftF(t->left);
        t = rotateRightF(t);

        switch (t->bf) {
            case EH:
                t->left->bf = EH;
                t->right->bf = EH;
                break;
            case LH:
                t->left->bf = EH;
                t->right->bf = RH;
                break;
            case RH:
                t->left->bf = LH;
                t->right->bf = EH;
        }
        t->bf = EH;
    }
    return t;

}

/* Insere um elemento numa árvore à direita */
static Freg insertRightF(Freg t, char *e, int valor, int *cresceu) {
    t->right = insertTreeF(t->right,e,valor,cresceu);

    if (*cresceu)
        switch (t->bf) {
            case LH:
                t->bf = EH;
                *cresceu = 0;
                break;
            case EH:
                t->bf = RH;
                *cresceu = 1;
                break;
            case RH:
                t = balanceRightF(t);
                *cresceu = 0;
        }
    return t;
}

/* Insere um elemento numa árvore à esquerda */
static Freg insertLeftF(Freg t, char *e, int valor, int *cresceu){
    t->left = insertTreeF(t->left,e,valor,cresceu);

    if (*cresceu)
        switch (t->bf) {
            case LH:
                t = balanceLeftF(t);
                *cresceu = 0;
                break;
            case EH:
                t->bf = LH;
                *cresceu = 1;
                break;
            case RH:
                t->bf = EH;
                *cresceu = 0;
        }
    return t;
}

/* Insere um elemento numa árvore AVL */
static Freg insertTreeF(Freg t, char *e, int valor, int *cresceu) {

    if (t==NULL) {
        t = (Freg)malloc(sizeof(struct treeFreg));
        t->freguesia = strdup(e);
        t->valor = valor;
        t->right = t->left = NULL;
        t->bf = EH;
        *cresceu = 1;

        if (!t) {
            printf("Erro1\n");
        }
    }

    else if (strcmp(e,t->freguesia)==0) {
        t->valor += valor;
        return t;
    }

    else if (strcmp(e,t->freguesia)>0)
        t = insertRightF(t,e,valor,cresceu);
    else
        t = insertLeftF(t,e,valor,cresceu);
    return t;
}

/* Roda uma árvore á direita */
static Conc rotateRightC(Conc t) {
    Conc aux;

    if ((!t) || (!t->left)) {
       printf("Erro21\n");
    }

    else {
        aux = t->left;
        t->left = aux->right;
        aux->right = t;
        t = aux;
    }
    return t;
}

/* Roda uma árvore á esquerda */
static Conc rotateLeftC(Conc t) {
    Conc aux;

    if ((! t)||(! t->right)) {
       printf("Erro22\n");    }

    else {
        aux = t->right;
        t->right = aux->left;
        aux->left = t;
        t = aux;
    }
    return t;
}

/* Roda uma árvore para que esta fique balanceada rotação a esquerda */
static Conc balanceRightC(Conc t) {

    if (t->right->bf==RH) {
        /* Rotacao simples a esquerda */
        t = rotateLeftC(t);
        t->bf = EH;
        t->left->bf = EH;
    }

    else {
        /* Dupla rotacao */
        t->right = rotateRightC(t->right);
        t = rotateLeftC(t);

        switch (t->bf) {
            case EH:
                t->left->bf = EH;
                t->right->bf = EH;
                break;
            case LH:
                t->left->bf = EH;
                t->right->bf = RH;
                break;
            case RH:
                t->left->bf = LH;
                t->right->bf = EH;
        }
        t->bf = EH;
    }
    return t;
}

/* Roda uma árvore para que esta fique balanceada rotação a direita*/
static Conc balanceLeftC(Conc t) {

    if (t->left->bf==LH) {
        /* Rotacao simples a direita */
        t = rotateRightC(t);
        t->bf = EH;
        t->right->bf = EH;
    }

    else {
        /* Dupla rotacao */
        t->left = rotateLeftC(t->left);
        t = rotateRightC(t);

        switch (t->bf) {
            case EH:
                t->left->bf = EH;
                t->right->bf = EH;
                break;
            case LH:
                t->left->bf = EH;
                t->right->bf = RH;
                break;
            case RH:
                t->left->bf = LH;
                t->right->bf = EH;
        }
        t->bf = EH;
    }
    return t;

}

/* Insere um elemento numa árvore à direita */
static Conc insertRightC(Conc t, char *e[], int valor, int *cresceu) {
    t->right = insertTreeC(t->right,e,valor,cresceu);

    if (*cresceu)
        switch (t->bf) {
            case LH:
                t->bf = EH;
                *cresceu = 0;
                break;
            case EH:
                t->bf = RH;
                *cresceu = 1;
                break;
            case RH:
                t = balanceRightC(t);
                *cresceu = 0;
        }
    return t;
}

/* Insere um elemento numa árvore à esquerda */
static Conc insertLeftC(Conc t, char *e[], int valor, int *cresceu){
    t->left = insertTreeC(t->left,e,valor,cresceu);

    if (*cresceu)
        switch (t->bf) {
            case LH:
                t = balanceLeftC(t);
                *cresceu = 0;
                break;
            case EH:
                t->bf = LH;
                *cresceu = 1;
                break;
            case RH:
                t->bf = EH;
                *cresceu = 0;
        }
    return t;
}

/* Insere um elemento numa árvore AVL */
static Conc insertTreeC(Conc t, char *e[], int valor, int *cresceu) {
    int c;

    if (t == NULL) {
        c = 0;

        t = (Conc)malloc(sizeof(struct treeConc));
        t->concelho = strdup(e[1]);
        t->freguesias = NULL;
        t->freguesias = insertTreeF(t->freguesias,e[2],valor,&c);
        t->agregadoC = valor;
        t->right = t->left = NULL;
        t->bf = EH;
        *cresceu = 1;

        if (!t) {
            printf("Erro2\n");
        }
    }

    else if (strcmp(e[1],t->concelho)==0) {
        c = 0;
        t->freguesias = insertTreeF(t->freguesias,e[2],valor,&c);
        t->agregadoC += valor;
        return t;
    }

    else if (strcmp(e[1],t->concelho)>0) {
        t = insertRightC(t,e,valor,cresceu);
    }
    else {
        t = insertLeftC(t,e,valor,cresceu);
    }
    return t;
}

/* ----------------------------------------------------------------------------------- */

/* Procura um concelho numa árvore */
static Conc procuraAVL_C(Conc t , char *concelho){
    Conc c;

    if(t != NULL){
        if(strcmp(concelho,t->concelho)==0) return t;

        else if(strcmp(concelho,t->concelho)>0){
            c = procuraAVL_C(t->right,concelho);
        }

        else{
            c = procuraAVL_C(t->left,concelho);
        }
    }
    else {
        return NULL;
    }
    return c;
}

/* Procura uma freguesia numa árvore */
static Freg procuraAVL_F(Freg t , char *freguesia){
    Freg c;

    if(t != NULL){
        if(strcmp(freguesia,t->freguesia)==0) return t;

        else if(strcmp(freguesia,t->freguesia)>0){
            c = procuraAVL_F(t->right,freguesia);
        }

        else{
            c = procuraAVL_F(t->left,freguesia);
        }
    }
    else {
        return NULL;
    }
    return c;
}


int getDist(char *dist){
    int i;

    for (i=0; i<livre; i++){
        if (strcmp(dist,distritos[i]->distrito) == 0) {
            return i;
        }
    }

    return -1;
}


int incrementar(char *listaNomes[], int valor) {
    int i;
    int cresceu=0;

    if (( i = getDist(listaNomes[0])) == -1){
        if (livre<18) {
            distritos[livre]= (Dist) malloc(sizeof(struct distrito));
            distritos[livre]->concelhos = NULL;
            distritos[livre]->distrito = strdup(listaNomes[0]);
            distritos[livre]->concelhos = insertTreeC(distritos[livre]->concelhos, listaNomes, valor, &cresceu);
            distritos[livre]->agregadoD = valor;

            livre++;

            return 0;
        }
    }
    else{
        distritos[i]->concelhos = insertTreeC(distritos[i]->concelhos,listaNomes,valor,&cresceu);
        distritos[i]->agregadoD += valor;
        return 0;
    }

    return 1;
}

void writeF(char *dist, char *conc, Freg t, int ficheiro) {
    int v;
    char str[20];

    if (t == NULL) return;
    writeF(dist,conc,t->left,ficheiro);

    write(ficheiro,dist,strlen(dist));
    write(ficheiro,":",sizeof(char));
    write(ficheiro,conc,strlen(conc));
    write(ficheiro,":",sizeof(char));
    write(ficheiro,t->freguesia,strlen(t->freguesia));
    write(ficheiro,":",sizeof(char));
    v = t->valor;
    sprintf(str, "%d", v);
    write(ficheiro,&str,strlen(str));
    write(ficheiro,"\n",sizeof(char));
    writeF(dist,conc,t->right,ficheiro);
}

void writeC (char *dist, Conc t, int ficheiro) {
    int v;
    char str[20];
    if (t == NULL) return;
    writeC(dist,t->left,ficheiro);

    write(ficheiro,dist,strlen(dist));
    write(ficheiro,":",sizeof(char));
    write(ficheiro,t->concelho,strlen(t->concelho));
    write(ficheiro,":",sizeof(char));
    v = t->agregadoC;
    sprintf(str, "%d", v);
    write(ficheiro,&str,strlen(str));
    write(ficheiro,"\n",sizeof(char));

    writeC(dist,t->right,ficheiro);
}
void writeCF (char *dist, Conc t, int ficheiro) {
    char str[20];
    int v;

    if (t == NULL) return;
    writeCF(dist,t->left,ficheiro);

    write(ficheiro,dist,strlen(dist));
    write(ficheiro,":",sizeof(char));
    write(ficheiro,t->concelho,strlen(t->concelho));
    write(ficheiro,":",sizeof(char));
    v = t->agregadoC;
    sprintf(str, "%d", v);
    write(ficheiro,&str,strlen(str));
    write(ficheiro,"\n",sizeof(char));

    writeF(dist,t->concelho,t->freguesias,ficheiro);

    writeCF(dist,t->right,ficheiro);
}


int agregar(char *listaNomes[], unsigned nivel, char *path) {
    int i, v;
    char str[20];
    int ficheiro;
    Conc t; Freg l;

    ficheiro = open(path, O_WRONLY | O_APPEND);

    if(ficheiro == -1) {
		creat(path, 0666);
		ficheiro = open(path, O_RDWR | O_APPEND);
	}

    if (( i = getDist(listaNomes[0]))!=-1){

        switch (nivel) {
            case 0:
                v = distritos[i]->agregadoD;
                write(ficheiro,distritos[i]->distrito,strlen(distritos[i]->distrito));
                write(ficheiro,":",sizeof(char));
                sprintf(str, "%d", v);
                write(ficheiro,str,strlen(str));
                write(ficheiro,"\n",sizeof(char));
                break;
            case 1:
                if (!listaNomes[1]) {
                    v = distritos[i]->agregadoD;
                    write(ficheiro,distritos[i]->distrito,strlen(distritos[i]->distrito));
                    write(ficheiro,":",sizeof(char));
                    sprintf(str, "%d", v);
                    write(ficheiro,str,strlen(str));
                    write(ficheiro,"\n",sizeof(char));
                    writeC(distritos[i]->distrito, distritos[i]->concelhos, ficheiro);
                }
                else{
                    write(ficheiro,distritos[i]->distrito,strlen(distritos[i]->distrito));
                    write(ficheiro,":",sizeof(char));
                    t = procuraAVL_C(distritos[i]->concelhos, listaNomes[1]);
                    write(ficheiro,t->concelho,strlen(t->concelho));
                    write(ficheiro,":",sizeof(char));
                    v = t->agregadoC;
                    sprintf(str, "%d", v);
                    write(ficheiro,str,strlen(str));
                    write(ficheiro,"\n",sizeof(char));
                }
                break;
            case 2:
                if(!listaNomes[2]){
                    if (!listaNomes[1]) {
                        v = distritos[i]->agregadoD;
                        write(ficheiro,distritos[i]->distrito,strlen(distritos[i]->distrito));
                        write(ficheiro,":",sizeof(char));
                        sprintf(str, "%d", v);
                        write(ficheiro,str,strlen(str));
                        write(ficheiro,"\n",sizeof(char));
                        writeCF(distritos[i]->distrito,distritos[i]->concelhos,ficheiro);
                    }
                    else{
                        t = procuraAVL_C(distritos[i]->concelhos, listaNomes[1]);
                        writeF(distritos[i]->distrito, t->concelho, t->freguesias, ficheiro);
                    }
                }
                else{
                    write(ficheiro,distritos[i]->distrito,strlen(distritos[i]->distrito));
                    write(ficheiro,":",sizeof(char));
                    t = procuraAVL_C(distritos[i]->concelhos, listaNomes[1]);
                    write(ficheiro,t->concelho,strlen(t->concelho));
                    write(ficheiro,":",sizeof(char));
                    l = procuraAVL_F(t->freguesias, listaNomes[2]);
                    write(ficheiro,l->freguesia,strlen(l->freguesia));
                    v = l->valor;
                    write(ficheiro,&v,sizeof(int));
                    write(ficheiro,"\n",sizeof(char));
                }
                break;
            default:
                break;
        }
    }
    close(ficheiro);
    return 0;
}
/*
void imprimeAVL_F (Freg t) {
    if (t == NULL) return;
    imprimeAVL_F(t->left);
    printf("%s - %d\n", t->freguesia, t->valor);
    imprimeAVL_F(t->right);
}

void imprimeAVL_C(Conc t) {
    if (t == NULL) return;
    imprimeAVL_C(t->left);
    printf("- - - %s", t->concelho);
    printf("- - - %d\n", t->agregadoC);
    imprimeAVL_F (t->freguesias);
    imprimeAVL_C(t->right);
}

void imprime(){
    int i = 0;
    while (i<livre) {
        printf("- - - - - - - %s", distritos[i]->distrito);
        printf("- - - %d\n", distritos[i]->agregadoD);
        imprimeAVL_C(distritos[i]->concelhos);
        i++;
    }
}
*/
