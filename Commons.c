# include "Commons.h"

// Writes a line to the fd
int writeLine (int fd, char *line) {
  char newLine = '\n';
  write(fd, line, strlen(line));
  write(fd, &newLine, 1);
  return 1;
}

/**
 * Reads from the fd one line
 * and reserves the space in memory.
 */
char *nextLine (int fd){
  char buffer[1];
  int s = 0;
  char *line = NULL;
  char *tmp = NULL;
  while (read(fd, buffer, 1) > 0) {
    if (buffer[0] == 0 || buffer[0] == '\n') {
      return line;
    } else {
      s++;
      tmp = line;
      line = (char *) malloc(sizeof(char) * (s+1));
      if (tmp != NULL) {
        strcpy(line, tmp);
        free(tmp);
      }
      line[s-1] = buffer[0];
      line[s] = '\0';
    }
  }
  return line;
}

/**
 * Returns:
 * # 1 if line starts with prefix
 * # 0 otherwise
 */
int startsWith(char *line, char *prefix){
    return strstr(line, prefix) == line;
}
