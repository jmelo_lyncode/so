# ifndef __COMMONS__
# define __COMMONS__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>

int writeLine (int fd, char *line);
int startsWith (char *line, char *prefix);
char * nextLine (int fd);

# endif
